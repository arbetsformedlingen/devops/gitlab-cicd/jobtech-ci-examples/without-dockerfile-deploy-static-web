import {Component, EventEmitter} from '@angular/core';
import {EducationLight} from "./model/searched-educations.model";
import {NavbarService} from "./services/navbar.service";
import {EducationMatchResultLight} from "./model/matched-educations-by-occupation.model";
import {version} from "../environments/version";
import {environment} from "../environments/environment";
import {ActivatedRoute} from "@angular/router";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {

  constructor(
    public navbarService: NavbarService,
    private activatedroute:ActivatedRoute
  ) { }

  // debug flag used to show/hide debug information in the GUI.
  // To activate debug, call the GUI with querystring debug=true,
  // for example: http://localhost:4200/?debug=true
  debug = false;

  sub = this.activatedroute.queryParamMap
    .subscribe(params => {
      this.debug = params.get('debug')?.toLowerCase() == 'true';
      console.log('this.debug:' + this.debug);
    });

  title = 'education-frontend-app';
  frontendVersion = version.frontendVersion;

  backendUrl = environment.settings.educationApi;

  educationLight: EventEmitter<EducationLight> = new EventEmitter<EducationLight>();

  educationResultLight: EventEmitter<EducationMatchResultLight> = new EventEmitter<EducationMatchResultLight>();

  updateEducationInfo(education: any) {
    this.educationLight.emit(education);
  }

  updateEducationResultInfo(education: any) {
    this.educationResultLight.emit(education);
  }

}
