import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MatchEducationsByOccupationDetailsComponent } from './match-educations-by-occupation-details.component';

describe('MatchEducationsByOccupationDetailsComponent', () => {
  let component: MatchEducationsByOccupationDetailsComponent;
  let fixture: ComponentFixture<MatchEducationsByOccupationDetailsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MatchEducationsByOccupationDetailsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MatchEducationsByOccupationDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
