import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MatchEducationsByOccupationResultComponent } from './match-educations-by-occupation-result.component';

describe('MatchEducationsByOccupationResultComponent', () => {
  let component: MatchEducationsByOccupationResultComponent;
  let fixture: ComponentFixture<MatchEducationsByOccupationResultComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MatchEducationsByOccupationResultComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MatchEducationsByOccupationResultComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
