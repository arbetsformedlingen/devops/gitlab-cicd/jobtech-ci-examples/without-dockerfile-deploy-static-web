import {AfterContentChecked, Component, EventEmitter, OnDestroy, OnInit, Output} from '@angular/core';
import {MatchEducationsByOccupationService} from "../services/match-educations-by-occupation.service";

import {EducationMatchResultLight, MatchedEducations} from "../model/matched-educations-by-occupation.model";
import {MatchEducationsByJobtitleQuery} from "../model/search-queries.model";
import {
  faAngleDown,
  faAngleUp,
  faExternalLinkAlt,
  faInfoCircle,
  faMehRollingEyes,
  faGlobe,
  faLocationDot,
  faLanguage,
  faClock
} from '@fortawesome/free-solid-svg-icons';
import {SearchparametersService} from "../services/searchparameters.service";
import {Searchparameter} from "../model/searchparameter.model";
import {AppComponent} from "../app.component";

@Component({
  selector: 'app-match-educations-by-occupation-result',
  templateUrl: './match-educations-by-occupation-result.component.html',
  styleUrls: ['./match-educations-by-occupation-result.component.scss']
})
export class MatchEducationsByOccupationResultComponent implements OnInit, OnDestroy, AfterContentChecked {


  constructor(
    public matchEducationsService: MatchEducationsByOccupationService,
    private searchparametersService: SearchparametersService,
    public appComponent: AppComponent
    // private navbarService: NavbarService
  ) { }



  faAngleDown = faAngleDown;
  faAngleUp = faAngleUp;
  faInfoCircle = faInfoCircle;
  faBackendError = faMehRollingEyes;
  faExternalLinkAlt = faExternalLinkAlt;
  faGlobe = faGlobe;
  faLocationDot = faLocationDot;
  faLanguage = faLanguage;
  faClock = faClock

  searchEducationsResult = new MatchedEducations()

  selectedEducation: EducationMatchResultLight | undefined = undefined;

  municipalities: Array<Searchparameter> | undefined;

  private latestSearch: MatchEducationsByJobtitleQuery | undefined = undefined;

  private educationInfoHeight: Number = 0;

  public showTotalHits: Boolean = false;

  @Output() public education: EventEmitter<EducationMatchResultLight> = new EventEmitter<EducationMatchResultLight>();

  ngOnInit(): void {
    this.loadMunicipalities();
    // this.navbarService.enableSearch();
  }


  loadMunicipalities() {
    this.searchparametersService.getMunicipalities().subscribe(items => {
      this.municipalities = items;
      const nrOfItems = this.municipalities.length;
      console.log('Loaded ' + nrOfItems + ' municipalities');
    });
  }

  getMunicipality(municipalityCode: string | undefined): string {
    if (municipalityCode && this.municipalities) {
      for (let municipality of this.municipalities) {
        if (municipality && municipality.key == municipalityCode && municipality.value) {
          return municipality.value;
        }
      }
    }
    return ""
  }

  getLanguage(languageShortening: string | undefined): string {
    if (languageShortening) {
      if (languageShortening == "swe") {
        return "svenska";
      }
      if (languageShortening == "eng") {
        return "engelska";
      }
      return languageShortening;
    }
    return "";
  }

  capitalizeWords(words: string | undefined): string{
    if (!words) {
      return "";
    }
    return words.replace(/(?:^|\s)\S/g,(res)=>{ return res.toUpperCase();});
  };

  getEducationsLight(): EducationMatchResultLight[] | undefined {
    return this._getSearchedEducations().educations;
  }





  _getSearchedEducations(): MatchedEducations {

    this.matchEducationsService.educationsBySearchResult.subscribe(searchres => {
      {
        this.searchEducationsResult = searchres;

        if (this.searchEducationsResult.searchQuery !== this.latestSearch) {
          this.setDefaulDetails();
          this.latestSearch = this.searchEducationsResult.searchQuery;

          Promise.resolve().then(() => this.showTotalHits = true);
          if (this.searchEducationsResult.educations && this.searchEducationsResult.educations.length > 0) {
            const dom_result_list_items = document.getElementById('result-list-items-container');
            if (dom_result_list_items !== null) {
              dom_result_list_items.scrollTo(0, 0);
            }
          }
        }

      }
    });
    return this.searchEducationsResult
  }

  private setDefaulDetails() {
    if (this.searchEducationsResult.educations && this.searchEducationsResult.educations.length > 0) {
      console.log('Setting selectedEducation to first item in result');
      this.selectedEducation = this.searchEducationsResult.educations[0];
      this.education.emit(this.selectedEducation);

    } else {
      this.education.emit(undefined);
    }
  }


  ngAfterContentChecked() {
    this.setResultListItemsContainerHeight();
  }

  private setResultListItemsContainerHeight() {
    // console.log('setResultListItemsContainerHeight')
    const educationInfoElement = document.getElementById('education-info-container');
    // console.log('educationInfoElement ' + educationInfoElement)
    const resultListItemsContainerElement = document.getElementById('result-list-items-container');
    // console.log('resultListItemsContainerElement ' + resultListItemsContainerElement)
    // If result-list-items-container has 100% width (small device), don't set the height with script.
    if (resultListItemsContainerElement !== null && window.innerWidth > resultListItemsContainerElement.offsetWidth) {
      if (educationInfoElement != null && educationInfoElement.offsetHeight !== this.educationInfoHeight) {
        const resultInformationElement = document.getElementById('result-information');
        if (resultInformationElement != null) {
          const newHeight = educationInfoElement.offsetHeight - (resultInformationElement.offsetHeight + 17);
          this.educationInfoHeight = educationInfoElement.offsetHeight;
          if (newHeight > 100) {
            // Avoid setting height too low.
            console.log('Setting result-list-items-container height to: ' + newHeight);
            resultListItemsContainerElement.style.height = newHeight + 'px';
          }
        }
      }
    }
  }

  ngOnDestroy(): void {
    // this.navbarService.disableSearch();
  }

  isLastPage(): boolean {
    return this.matchEducationsService.isLastPage;
  }

  onResultItemsScroll(): void {
    console.log('onResultItemsScroll')
    if (this.isLastPage()) {
      console.log('Is last page');
      return;
    }
    if (!this.matchEducationsService.isLoadingPageResult) {
      console.log('onResultItemsScroll, loading more result (previous page: ' + this.matchEducationsService.page + ')');
      this.loadMoreEducations();
    }
    else {
      console.log('is loading page result')
    }
  }

  private loadMoreEducations() {
    this.matchEducationsService.fetchMoreSearchEducations();
  }

  setEducationInfo(education: EducationMatchResultLight): void {
    this.selectedEducation = education;
    if (education) {
      console.log('this.education.emit for education: ' + education.id + ', ' + education.title);
      this.education.emit(education);
    }
  }

  totalCount(): Number {
    if (this.searchEducationsResult) {
      return <Number>this.searchEducationsResult.totalPages;
    }
    else {
      return 0;
    }

  }


}
