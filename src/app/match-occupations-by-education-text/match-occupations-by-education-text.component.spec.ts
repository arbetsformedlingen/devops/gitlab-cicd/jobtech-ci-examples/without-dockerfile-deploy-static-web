import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MatchOccupationsByEducationTextComponent } from './match-occupations-by-education-text.component';

describe('MatchOccupationsByEducationTextComponent', () => {
  let component: MatchOccupationsByEducationTextComponent;
  let fixture: ComponentFixture<MatchOccupationsByEducationTextComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MatchOccupationsByEducationTextComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MatchOccupationsByEducationTextComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
