import { Component, OnInit } from '@angular/core';
import {UntypedFormControl, UntypedFormGroup} from "@angular/forms";
import {MatchOccupationsByEducationTextQuery} from "../model/search-queries.model";
import {MatchOccupationsByEducationTextService} from "../services/match-occupations-by-education-text.service";
import {MatSliderChange} from "@angular/material/slider";

@Component({
  selector: 'app-match-occupations-by-education-text',
  templateUrl: './match-occupations-by-education-text.component.html',
  styleUrls: ['./match-occupations-by-education-text.component.scss']
})
export class MatchOccupationsByEducationTextComponent implements OnInit {
  form: UntypedFormGroup = new UntypedFormGroup({
    freeTextSearchCtrl: new UntypedFormControl(''),
    headingCtrl: new UntypedFormControl('')
  })
  defaultNumberOfMatches: number = 10;
  maxNumberOfMatches: number = this.defaultNumberOfMatches;
  freeTextSearchCtrl: UntypedFormControl;
  headingCtrl: UntypedFormControl;


  constructor(
    private matchOccupationsByEducationtextService: MatchOccupationsByEducationTextService,
  ) {
    this.freeTextSearchCtrl = new UntypedFormControl();
    this.headingCtrl = new UntypedFormControl();
  }

  maxNumberOfMatchingLabel(): string {
    return this.maxNumberOfMatches.toString();
  }

  ngOnInit(): void {
  }

  formatLabel(value: number): string {
    if (value) {
      return value.toString();
    }
    else {
      return "";
    }
  }

  updateChangeMaxNumberOfMatches(event: any) {
    this.search();
  }


  isValidControls() {
    return this.freeTextSearchCtrl.valid;
  }

  search(): void {
    if (!this.isValidControls()) {
      return;
    }

    const query: MatchOccupationsByEducationTextQuery = {
      input_text: '',
      input_headline: '',
      limit: this.maxNumberOfMatches,
      offset: 0,
      include_metadata: true
    }
    if (!this.freeTextSearchCtrl.value && !this.headingCtrl.value){
      return;
    }
    if (this.freeTextSearchCtrl.value) {
      query.input_text = this.freeTextSearchCtrl.value;
    }
    if (this.headingCtrl.value) {
      query.input_headline = this.headingCtrl.value;
    }

    console.log('MatchOccupationsByEducationTextQuery:' + JSON.stringify(query));

    this.matchOccupationsByEducationtextService.getMatchedOccupations(query)
  }


}
