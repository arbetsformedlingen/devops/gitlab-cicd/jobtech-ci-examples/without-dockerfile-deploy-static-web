export class EnrichedOccupation {
  id: string | undefined;
  occupationLabel: string | undefined;
  conceptTaxonomyId: string | undefined;
  legacyAmsTaxonomyId: string | undefined;

  metadata: EnrichedOccupationMetadata | undefined;
}

export class EnrichedOccupationMetadata {
  enriched_ads_count: number | undefined;
  enriched_ads_percent_of_total: number | undefined;
  competencies_metadata: TermMetadata[] | undefined;
  occupations_metadata: TermMetadata[] | undefined;
}

export class TermMetadata {
  term: string | undefined;
  percent_for_occupation: number | undefined;
}
