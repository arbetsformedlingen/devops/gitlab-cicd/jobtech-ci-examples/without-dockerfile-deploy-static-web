export class OccupationSalaryStatistics {
  ssyk: string | undefined;
  salaryStatistics = new Array<SalaryStatistics>();
}

export class SalaryStatistics {

  // TODO Lookup name of region
  region: string | undefined;
  sector: string | undefined;
  gender: string | undefined;
  year: string | undefined;
  basicSalary: string | undefined;
  monthlySalary: string | undefined;
  numberOfEmployees: string | undefined;
}
