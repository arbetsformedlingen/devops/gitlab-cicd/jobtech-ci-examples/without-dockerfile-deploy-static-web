
export class SearchEducationsQuery {
  freeText: string | undefined;
  educationType: string | undefined;
  educationForm: string | undefined;
  municipalityCode: string | undefined;
  regionCode: string | undefined;
  educationCode: string | undefined;
  paceOfStudyPercentage: string | undefined;
  educationPlanExists: boolean | undefined;
  distance: boolean | undefined;
  limit: number | undefined;
  offset: number | undefined;
}

export class MatchEducationsByJobtitleQuery {
  jobtitle: string | undefined;
  education_type: Array<string> | undefined;
  education_form: Array<string> | undefined;
  municipality_code: string | undefined;
  region_code: string | undefined;
  distance: boolean | undefined;
  limit: number | undefined;
  offset: number | undefined;
  include_metadata: boolean | undefined;
}

export class MatchOccupationsByEducationQuery {
  education_id: string | undefined;
  limit: number | undefined;
  offset: number | undefined;
  include_metadata: boolean | undefined;
}

export class MatchOccupationsByEducationTextQuery {
  input_text: string | undefined;
  input_headline: string | undefined;
  limit: number | undefined;
  offset: number | undefined;
  include_metadata: boolean | undefined;
}
