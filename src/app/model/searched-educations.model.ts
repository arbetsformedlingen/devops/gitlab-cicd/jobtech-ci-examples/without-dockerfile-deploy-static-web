import {SearchEducationsQuery} from "./search-queries.model";

export class SearchedEducations {
  totalHits: number | undefined;
  totalPages: number | undefined;
  pageSize: number | undefined;
  page: number | undefined;

  searchQuery: SearchEducationsQuery | undefined;

  educations: EducationLight[] | undefined;
}

export class EducationLight {
  identifier: string | undefined;
  code: string | undefined;
  description: string | undefined;
  title: string | undefined;
  type: string | undefined;
  form: string | undefined;
  providers = new Array();
  municipalityCodes = new Array();
  regionCodes = new Array();
  languagesOfInstruction = new Array();
  onlyAsPartOfPrograms = new Array();
  paceOfStudyPercentages = new Array();
  timeOfStudies = new Array();
  tuitionFees = new Array();
  distance : boolean = false;
}
