import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SearchEducationDetailsComponent } from './search-education-details.component';

describe('SearchEducationDetailsComponent', () => {
  let component: SearchEducationDetailsComponent;
  let fixture: ComponentFixture<SearchEducationDetailsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SearchEducationDetailsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SearchEducationDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
