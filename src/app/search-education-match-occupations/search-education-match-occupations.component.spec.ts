import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SearchEducationMatchOccupationsComponent } from './search-education-match-occupations.component';

describe('SearchEducationMatchOccupationsComponent', () => {
  let component: SearchEducationMatchOccupationsComponent;
  let fixture: ComponentFixture<SearchEducationMatchOccupationsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SearchEducationMatchOccupationsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SearchEducationMatchOccupationsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
