import {Component, EventEmitter, Input, OnChanges, OnInit} from '@angular/core';
import {EducationLight} from "../model/searched-educations.model";
import {Education} from "../model/searched-education.model";
import {MatchedOccupations, RelatedOccupation} from "../model/matched-occupations.model";
import {MatchOccupationsService} from "../services/match-occupations.service";
import {NavbarService} from "../services/navbar.service";
import {FetchOccupationForecastsService} from "../services/fetch-occupation-forecasts.service";
import {Forecast} from "../model/occupation-forecasts.model";
import {FetchSalaryStatisticsService} from "../services/fetch-salary-statistics.service";
import {OccupationSalaryStatistics} from "../model/occupation-salary-statistics.model";
import {NgxGaugeCap, NgxGaugeType} from "ngx-gauge/gauge/gauge";
import {MatchOccupationsByEducationQuery} from "../model/search-queries.model";
import {AppComponent} from "../app.component";

@Component({
  selector: 'app-search-education-match-occupations',
  templateUrl: './search-education-match-occupations.component.html',
  styleUrls: ['./search-education-match-occupations.component.scss']
})
export class SearchEducationMatchOccupationsComponent implements OnInit, OnChanges {

  @Input() educationInput: EventEmitter<EducationLight> | undefined;

  educationLight: EducationLight | undefined;
  education: Education | undefined;
  matchedOccupations: MatchedOccupations | undefined;

  public salaryChartMap = new Map<string, any>();
  public employeesChartMap = new Map<string, any>();

  gaugeType: NgxGaugeType = 'arch';
  gaugeMin = 1;
  gaugeMax = 9;
  gaugeThick = 12;
  gaugeSize = 130;
  gaugeCap: NgxGaugeCap = 'round';
  gaugeThresholdConfig = {
    '1': {color: 'red'},
    '5': {color: '#d0ff22'},
    '9': {color: 'green'}
  };
  FORECAST_LOW: number = 2;
  FORECAST_AVG: number = 5;
  FORECAST_HIGH: number = 9;

  constructor(
    public matchOccupationsService: MatchOccupationsService,
    private fetchOccupationForecastsService: FetchOccupationForecastsService,
    private fetchSalaryStatisticsService: FetchSalaryStatisticsService,
    public navbarService: NavbarService,
    public appComponent: AppComponent)
  { }

  ngOnInit(): void {
  }

  lowercaseWords(words: string | undefined): string{
    if (!words) {
      return "";
    }
    return words.split(/\s+/).map(word => {
      return word.charAt(0).toUpperCase() + word.slice(1).toLowerCase();
    }).join(' ');
  };

  ngOnChanges(): void {
    this.subscribeChanges()
  }

  getGaugeValueForForecast(forecast: Forecast): number {
    if (forecast.demandValue) {
      if (forecast.demandValue <= 3.0) {
        return this.FORECAST_LOW;
      }
      if (forecast.demandValue == 4.0) {
        return this.FORECAST_AVG;
      }
      if (forecast.demandValue >= 5.0) {
        return this.FORECAST_HIGH;
      }
    }
    return 0;
  }

  getGaugeValueLabelForForecast(forecast: Forecast): string {
    const demandValue = this.getGaugeValueForForecast(forecast);
    if (demandValue == this.FORECAST_LOW) {
      return 'Låg';
    }
    if (demandValue == this.FORECAST_AVG) {
      return 'I balans';
    }
    if (demandValue == this.FORECAST_HIGH) {
      return 'Hög';
    }
    else {
      return ''
    }
  }

  getGaugeLabelForForecast(forecast: Forecast): string {
    if (forecast.year) {
      return 'Prognos för ' + (forecast.year).toString();
    }
    return '';

  }

  getRelatedOccupations(): RelatedOccupation[] | undefined {
    if (this.matchedOccupations && this.matchedOccupations.relatedoccupations) {
      return this.matchedOccupations.relatedoccupations;
    }
    else {
      return undefined
    }
  }

  showSalaryCharts(relatedOccupation: RelatedOccupation | undefined): boolean {
    if (relatedOccupation && relatedOccupation.salary && relatedOccupation.salary.ssyk) {
      const array = this.salaryChartMap.get(relatedOccupation.salary.ssyk);
      if (array && array.length > 0) {
        return true;
      }
    }
    return false;
  }

  getSalaryCharts(relatedOccupation: RelatedOccupation | undefined): any {
    if (relatedOccupation && relatedOccupation.salary && relatedOccupation.salary.ssyk) {
      return this.salaryChartMap.get(relatedOccupation.salary.ssyk);
    }
    else {
      return [];
    }
  }

  salaryLegend = false;
  salaryShowLabels = true;
  salaryAnimations = true;
  salaryXAxis = true;
  salaryYAxis = true;
  salaryShowYAxisLabel = true;
  salaryShowXAxisLabel = false;
  salaryXAxisLabel = 'År';
  salaryYAxisLabel = 'Lön';
  salaryTimeline = true;
  salaryColorScheme = {
    domain: ['#1616b2']
  };

  showEmployeesCharts(relatedOccupation: RelatedOccupation | undefined): boolean {
    if (relatedOccupation && relatedOccupation.salary && relatedOccupation.salary.ssyk) {
      const array = this.employeesChartMap.get(relatedOccupation.salary.ssyk);
      if (array && array.length > 0) {
        return true;
      }
    }
    return false;
  }

  getEmployeesCharts(relatedOccupation: RelatedOccupation | undefined): any {
    if (relatedOccupation && relatedOccupation.salary && relatedOccupation.salary.ssyk) {
      return this.employeesChartMap.get(relatedOccupation.salary.ssyk);
    }
    else {
      return [];
    }
  }

  employeesLegend = false;
  employeesShowLabels = true;
  employeesAnimations = true;
  employeesXAxis = true;
  employeesYAxis = true;
  employeesShowYAxisLabel = true;
  employeesShowXAxisLabel = false;
  employeesXAxisLabel = 'År';
  employeesYAxisLabel = 'Antal anställda';
  employeesTimeline = true;
  employeesColorScheme = {
    domain: ['#1616b2']
  };

  updateSalaryChartData(salaryStatistics: OccupationSalaryStatistics | undefined) {
  let chartData = [];
  if (salaryStatistics && salaryStatistics.ssyk && salaryStatistics.salaryStatistics && salaryStatistics.salaryStatistics.length > 0) {
    let series = [];

    for (let salaryStatistic of salaryStatistics.salaryStatistics) {
      if (salaryStatistic.monthlySalary && salaryStatistic.monthlySalary != '..') {
        series.push({name: salaryStatistic.year, value: salaryStatistic.monthlySalary});
      }
    }
    chartData.push({name: 'Genomsnittlig månadslön', series: series});
    if (series.length > 0) {
      this.salaryChartMap.set(salaryStatistics.ssyk, chartData);
    }
    else {
      this.salaryChartMap.set(salaryStatistics.ssyk, {})
    }
  }
  }

  updateEmployeesChartData(salaryStatistics: OccupationSalaryStatistics | undefined) {
    let chartData = [];
    if (salaryStatistics && salaryStatistics.ssyk && salaryStatistics.salaryStatistics && salaryStatistics.salaryStatistics.length > 0) {
      let series = [];

      for (let salaryStatistic of salaryStatistics.salaryStatistics) {
        if (salaryStatistic.numberOfEmployees && salaryStatistic.numberOfEmployees != '..') {
          series.push({name: salaryStatistic.year, value: salaryStatistic.numberOfEmployees});
        }
      }
      chartData.push({name: 'Antal anställda', series: series});
      if (series.length > 0) {
        this.employeesChartMap.set(salaryStatistics.ssyk, chartData);
      }
      else {
       this.employeesChartMap.set(salaryStatistics.ssyk, {})
      }
    }
  }

  getTitle(educationLight: EducationLight | undefined): string {
    let title = ""
    if (educationLight) {
      if (educationLight.title) {
        title = title + "Relaterade yrken till utbildningen <span class='education-title'>" + this.lowercaseWords(educationLight.title) + "</span>";
      }
      if (educationLight.code) {
        title = title + " (" + educationLight.code +  ")";
      }
    }
    return title;
  }

  subscribeChanges() {
    // @ts-ignore
    this.educationInput.subscribe((educationInput) => {
      this.educationLight = educationInput;
      this.salaryChartMap = new Map<string, any>();
      this.employeesChartMap = new Map<string, any>();
      if (!educationInput) {
        this.education = undefined;
      } else if (educationInput.identifier) {

        const query: MatchOccupationsByEducationQuery = {
          education_id: educationInput.identifier,
          limit: 15,
          offset: 0,
          include_metadata: false
        }

        this.matchOccupationsService.getMatchedOccupations(query).subscribe(
          matchedEducations => {
            if (!matchedEducations) {
              this.matchedOccupations = undefined;
            }
            else {
              this.matchedOccupations = matchedEducations;
              if (this.matchedOccupations && this.matchedOccupations.relatedoccupations) {
                for (let matchedOccupation of this.matchedOccupations.relatedoccupations) {
                  if (matchedOccupation.occupationGroup && matchedOccupation.occupationGroup.ssyk) {
                    this.fetchOccupationForecastsService.fetchOccupationForecasts(matchedOccupation.occupationGroup.ssyk).subscribe(
                      occupationForecasts => {
                        if (occupationForecasts) {
                          matchedOccupation.forecast = occupationForecasts;
                        }
                      }
                    );
                    this.fetchSalaryStatisticsService.fetchOccupationSalaryStatistics(matchedOccupation.occupationGroup.ssyk).subscribe(
                      occupationSalary => {
                        if (occupationSalary) {
                          matchedOccupation.salary = occupationSalary;
                          this.updateSalaryChartData(occupationSalary);
                          this.updateEmployeesChartData(occupationSalary)
                        }
                      }
                    );
                  }
                }
              }
            }
          }
        );
      }
    });
  }
}
