import {AfterContentChecked, AfterViewInit, Component, EventEmitter, OnDestroy, OnInit, Output} from '@angular/core';
import {EducationLight, SearchedEducations} from "../model/searched-educations.model";
import {SearchEducationsService} from "../services/search-educations.service";
import {NavbarService} from "../services/navbar.service";
import {
  faAngleDown,
  faAngleUp,
  faExternalLinkAlt,
  faInfoCircle,
  faMehRollingEyes,
  faGlobe,
  faLocationDot,
  faLanguage,
  faClock
} from '@fortawesome/free-solid-svg-icons';
import {SearchEducationsQuery} from "../model/search-queries.model";
import {Searchparameter} from "../model/searchparameter.model";
import {SearchparametersService} from "../services/searchparameters.service";

@Component({
  selector: 'app-search-education-result',
  templateUrl: './search-education-result.component.html',
  styleUrls: ['./search-education-result.component.scss']
})
export class SearchEducationResultComponent implements OnInit, OnDestroy, AfterViewInit, AfterContentChecked {

  constructor(
    public educationsService: SearchEducationsService,
    private navbarService: NavbarService,
    private searchparametersService: SearchparametersService
  ) { }

  ngAfterViewInit(): void {
    }

  faAngleDown = faAngleDown;
  faAngleUp = faAngleUp;
  faInfoCircle = faInfoCircle;
  faBackendError = faMehRollingEyes;
  faExternalLinkAlt = faExternalLinkAlt;
  faGlobe = faGlobe;
  faLocationDot = faLocationDot;
  faLanguage = faLanguage;
  faClock = faClock

  searchEducationsResult = new SearchedEducations()

  selectedEducation: EducationLight | undefined = undefined;

  municipalities: Array<Searchparameter> | undefined;

  private latestSearch: SearchEducationsQuery | undefined = undefined;

  private educationInfoHeight: Number = 0;

  public showTotalHits: Boolean = false;

  @Output() public education: EventEmitter<EducationLight> = new EventEmitter<EducationLight>();

  ngOnInit(): void {
    this.navbarService.enableSearch();
    this.loadMunicipalities();
  }

  loadMunicipalities() {
    this.searchparametersService.getMunicipalities().subscribe(items => {
      this.municipalities = items;
      const nrOfItems = this.municipalities.length;
      console.log('Loaded ' + nrOfItems + ' municipalities');
    });
  }

  getMunicipality(municipalityCode: string | undefined): string {
    if (municipalityCode && this.municipalities) {
      for (let municipality of this.municipalities) {
        if (municipality && municipality.key == municipalityCode && municipality.value) {
          return municipality.value;
        }
      }
    }
    return ""
  }

  getLanguage(languageShortening: string | undefined): string {
    if (languageShortening) {
      if (languageShortening == "swe") {
        return "svenska";
      }
      if (languageShortening == "eng") {
        return "engelska";
      }
      return languageShortening;
    }
    return "";
  }

  capitalizeWords(words: string | undefined): string{
    if (!words) {
      return "";
    }
    return words.replace(/(?:^|\s)\S/g,(res)=>{ return res.toUpperCase();});
  };

  getEducationsLight(): EducationLight[] | undefined {
    return this._getSearchedEducations().educations;
  }

  _getSearchedEducations(): SearchedEducations {
    this.educationsService.educationsBySearchResult.subscribe(searchres => {
      {
        this.searchEducationsResult = searchres
        if (this.searchEducationsResult.searchQuery !== this.latestSearch) {
          this.setDefaulDetails();
          this.latestSearch = this.searchEducationsResult.searchQuery;
          Promise.resolve().then(() => this.showTotalHits = true);
          if (this.searchEducationsResult.educations && this.searchEducationsResult.educations.length > 0) {
            const dom_result_list_items = document.getElementById('result-list-items-container');
            if (dom_result_list_items !== null) {
              dom_result_list_items.scrollTo(0, 0);
            }
          }
        }
      }
    });
    return this.searchEducationsResult
  }

  private setDefaulDetails() {
    this.navbarService.displayMatchedOccupations = true
    this.navbarService.displayEducationDetails = false
    if (this.searchEducationsResult.educations && this.searchEducationsResult.educations.length > 0) {
      console.log('Setting selectedEducation to first item in result');
      this.selectedEducation = this.searchEducationsResult.educations[0];
      this.education.emit(this.selectedEducation);

    } else {
      this.education.emit(undefined);
    }
  }


  ngAfterContentChecked() {
    this.setResultListItemsContainerHeight();
  }

  private setResultListItemsContainerHeight() {
    // console.log('setResultListItemsContainerHeight')
    const educationInfoElement = document.getElementById('education-info-container');
    // console.log('educationInfoElement ' + educationInfoElement)
    const resultListItemsContainerElement = document.getElementById('result-list-items-container');
    // console.log('resultListItemsContainerElement ' + resultListItemsContainerElement)
    // If result-list-items-container has 100% width (small device), don't set the height with script.
    if (resultListItemsContainerElement !== null && window.innerWidth > resultListItemsContainerElement.offsetWidth) {
      if (educationInfoElement != null && educationInfoElement.offsetHeight !== this.educationInfoHeight) {
        const resultInformationElement = document.getElementById('result-information');
        if (resultInformationElement != null && resultListItemsContainerElement != null) {
          const newHeight = educationInfoElement.offsetHeight - (resultInformationElement.offsetHeight + 17);
          this.educationInfoHeight = educationInfoElement.offsetHeight;
          if (newHeight > 100) {
            // Avoid setting height too low.
            console.log('Setting result-list-items-container height to: ' + newHeight);
            resultListItemsContainerElement.style.height = newHeight + 'px';
          }
        }
      }
    }
  }

  ngOnDestroy(): void {
    this.navbarService.disableSearch();
  }

  isLastPage(): boolean {
    return this.educationsService.isLastPage;
  }

  onResultItemsScroll(): void {
    console.log('onResultItemsScroll')
    if (this.isLastPage()) {
      console.log('Is last page');
      return;
    }
    if (!this.educationsService.isLoadingPageResult) {
      console.log('onResultItemsScroll, loading more result (previous page: ' + this.educationsService.page + ')');
      this.loadMoreEducations();
    }
    else {
      console.log('is loading page result')
    }
  }

  private loadMoreEducations() {
    this.educationsService.fetchMoreSearchEducations();
  }

  setEducationInfo(education: EducationLight): void {
    this.navbarService.displayMatchedOccupations = false
    this.navbarService.displayEducationDetails = true
    this.selectedEducation = education;
    if (education) {
      this.education.emit(education);
    }
  }

  setMatchedOccupations(education: EducationLight): void {
    this.navbarService.displayMatchedOccupations = true
    this.navbarService.displayEducationDetails = false
    this.selectedEducation = education;
    if (education) {
      this.education.emit(education);
    }
  }

  totalCount(): Number {
    if (this.searchEducationsResult) {
      return <Number>this.searchEducationsResult.totalPages;
    }
    else {
      return 0;
    }

  }

  // getGrowthPercent(employerlight: EmployerLight): Number {
  //   let retVal;
  //   if (employerlight.hasPredictions()) {
  //     retVal = employerlight.predictions.man12_rel;
  //     // Avoid zero (0.0) since Javascript considers 0 as False in a boolean statement.
  //     if (retVal === 0.0) {
  //       retVal = retVal + 0.0000000001;
  //     }
  //   } else {
  //     retVal = undefined;
  //   }
  //
  //   return retVal;
  // }

}
