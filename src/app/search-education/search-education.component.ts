import { Component, OnInit } from '@angular/core';
import { merge, Observable, of, reduce, startWith } from 'rxjs';
import { SearchEducationsQuery } from "../model/search-queries.model";
import {AbstractControl, FormControl, UntypedFormControl} from "@angular/forms";
import { SearchEducationsService } from "../services/search-educations.service";
import { SearchparametersService } from "../services/searchparameters.service";
import { NavbarService } from "../services/navbar.service";
import { AppComponent } from "../app.component";
import { map } from "rxjs/operators";
import { Searchparameter } from "../model/searchparameter.model";
import { MatCheckboxChange } from "@angular/material/checkbox";

@Component({
  selector: 'app-search-education',
  templateUrl: './search-education.component.html',
  styleUrls: ['./search-education.component.scss']
})
export class SearchEducationComponent implements OnInit {
  freeTextSearchCtrl: UntypedFormControl;
  geoCodeFilterCtrl: FormControl;
  educationCodeFilterCtrl: UntypedFormControl;
  paceOfStudyPercentageFilterCtrl: UntypedFormControl;
  educationPlanExistsSearchCtrl: UntypedFormControl
  distanceSearchCtrl: UntypedFormControl

  selectedEducationType: string;
  selectedEducationForm: string;
  educationTypes: Array<Searchparameter>;
  educationForms: Array<Searchparameter>;
  geos: Array<Searchparameter>;
  filteredGeos: Observable<Searchparameter[]>;
  MUNICIPALITY_CODE_LENGTH = 4
  REGION_CODE_LENGTH = 2

  constructor(
    private educationsService: SearchEducationsService,
    private navbarService: NavbarService,
    private searchparametersService: SearchparametersService,
    public appComponent: AppComponent
  ) {
    this.freeTextSearchCtrl = new UntypedFormControl();

    this.educationCodeFilterCtrl = new UntypedFormControl();
    this.paceOfStudyPercentageFilterCtrl = new UntypedFormControl();
    this.educationPlanExistsSearchCtrl = new UntypedFormControl();
    this.distanceSearchCtrl = new UntypedFormControl();
    this.geoCodeFilterCtrl = new FormControl<string>('', [
      CustomValidators.forbiddenObjectValidator
    ]);

    this.selectedEducationType = '';
    this.selectedEducationForm = '';
    this.filteredGeos = new Observable<Searchparameter[]>();
    this.educationTypes = new Array<Searchparameter>();
    this.educationForms = new Array<Searchparameter>();
    this.geos = new Array<Searchparameter>();

  }
  ngOnInit(): void {

    this.loadSearchparameters();

    this.filteredGeos = this.geoCodeFilterCtrl.valueChanges.pipe(
      startWith(''),
      map(value => this._filter(value)),
    );

  }

  displayFnGeo(geo: any): string {
    return geo && geo.value ? geo.value : '';
  }

  private _filter(value: string): Searchparameter[] {
    if (typeof value === "object") {
      return []
    }

    const filterValue = value.toLowerCase();
    return this.geos.filter(option => option.value?.toLowerCase().startsWith(filterValue));
  }

  private loadSearchparameters() {
    this.searchparametersService.getEducationTypes().subscribe(items => {
      this.educationTypes = items;
    });

    this.searchparametersService.getEducationForms().subscribe(items => {
      this.educationForms = items;
    });

    /* Merge search parameters for municipalites and regions as geos... */
    merge(this.searchparametersService.getMunicipalities(), this.searchparametersService.getRegions()).pipe(reduce((m, r) => m.concat(r))).subscribe(items => {
      this.geos = items;
      const nrOfItems = this.geos.length;
      console.log('Loaded ' + nrOfItems + ' geos');
    });

  }

  onChangeCheckBox(observable: MatCheckboxChange) {
    // Checkbox value changed. Ignore observable, just call search function...
    this.search();
  }

  search(): void {
    const searchEducationsQuery: SearchEducationsQuery = {
      freeText: '',
      educationType: '',
      educationForm: '',
      municipalityCode: '',
      regionCode: '',
      educationCode: '',
      paceOfStudyPercentage: '',
      distance: false,
      educationPlanExists: false,
      limit: 10,
      offset: 0
    }
    this.navbarService.displayEducationDetails = false
    this.navbarService.displayMatchedOccupations = false
    if (this.freeTextSearchCtrl.value) {
      searchEducationsQuery.freeText = this.freeTextSearchCtrl.value;
    }

    if (this.selectedEducationType !== "") {
      searchEducationsQuery.educationType = this.selectedEducationType;
    }

    if (this.selectedEducationForm !== "") {
      searchEducationsQuery.educationForm = this.selectedEducationForm;
    }
    if (this.geoCodeFilterCtrl.value) {
      const geoKey = this.geoCodeFilterCtrl.value.key;
      if (geoKey.length == this.REGION_CODE_LENGTH) {
        searchEducationsQuery.regionCode = geoKey;
      }
      else if (geoKey.length == this.MUNICIPALITY_CODE_LENGTH) {
        searchEducationsQuery.municipalityCode = geoKey;
      }
      else {
        console.warn('Unknown geo type selected ' + geoKey)
      }
    }

    if (this.distanceSearchCtrl.value) {
      searchEducationsQuery.distance = true;
    }

    if (this.educationPlanExistsSearchCtrl.value) {
      searchEducationsQuery.educationPlanExists = true;
    }


    console.log('searchEducationsQuery:' + JSON.stringify(searchEducationsQuery));

    this.educationsService.searchEducations(searchEducationsQuery, false)
  }

}

export class CustomValidators {

  static forbiddenObjectValidator(control: AbstractControl): any {
    // Validate if value is an object (and not e.g. a string value):
    if (control.value) {
      return typeof control.value !== 'object' || control.value === null ? { 'forbiddenObject': true } : null;
    }
    return;
  }
}
