import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SelectOccupationMatchEducationsComponent } from './select-occupation-match-educations.component';

describe('SelectOccupationMatchEducationsComponent', () => {
  let component: SelectOccupationMatchEducationsComponent;
  let fixture: ComponentFixture<SelectOccupationMatchEducationsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SelectOccupationMatchEducationsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SelectOccupationMatchEducationsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
