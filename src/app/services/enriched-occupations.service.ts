import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from "../../environments/environment";
import {Observable} from "rxjs";
import {EnrichedOccupation, EnrichedOccupationMetadata, TermMetadata} from "../model/enriched-occupation";
import {map} from "rxjs/operators";

@Injectable({
  providedIn: 'root'
})
export class EnrichedOccupationsService {
  private backendUrl = environment.settings.educationApi;

  constructor(private httpClient: HttpClient) {
  }

  public getEnrichedOccupation(occupationId: string, includeMetadata: boolean): Observable<EnrichedOccupation> {
    const url = `${this.backendUrl}/v1/enriched_occupations?occupation_id=${occupationId}&include_metadata=${includeMetadata}`;

    return this.httpClient
      .get(url)
      .pipe(map(data => this.handleEnrichedOccupationResponse(data)));

  }

  public handleEnrichedOccupationResponse(occupationIn: any): EnrichedOccupation {
    const occupationOut = new EnrichedOccupation()
    occupationOut.id = occupationIn['id']
    occupationOut.occupationLabel = occupationIn['occupation_label']

    const metadataOut = new EnrichedOccupationMetadata();

    metadataOut.competencies_metadata = EnrichedOccupationsService.populateTermsMetadata(occupationIn, 'competencies');
    metadataOut.occupations_metadata = EnrichedOccupationsService.populateTermsMetadata(occupationIn, 'occupations');

    occupationOut.metadata = metadataOut;

    return occupationOut;
  }

  private static populateTermsMetadata(occupationIn: any, typeName: string) {
    let termMetadatas: TermMetadata[] = new Array<TermMetadata>()

    const metadataIn = occupationIn['metadata'];
    const enrichedCandidatesTermFrequency = metadataIn['enriched_candidates_term_frequency'];
    const candidatesTermFrequencyForTypeIn = enrichedCandidatesTermFrequency[typeName];

    candidatesTermFrequencyForTypeIn.forEach((termMetadataIn: { term: string; percent_for_occupation: number }) => {
      const termMetadata = new TermMetadata();
      termMetadata.term = termMetadataIn['term'];
      termMetadata.percent_for_occupation = EnrichedOccupationsService.formatPercent(termMetadataIn['percent_for_occupation']);

      termMetadatas.push(termMetadata);
    });

    return termMetadatas;
  }

  private static formatPercent(numberToFormat: number) {
      return Math.round((numberToFormat + Number.EPSILON) * 1000) / 1000;
  }
}

