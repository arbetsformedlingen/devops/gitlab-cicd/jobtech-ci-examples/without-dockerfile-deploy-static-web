import {Injectable} from "@angular/core";
import {HttpClient} from "@angular/common/http";
import {environment} from "../../environments/environment";
import {Observable} from "rxjs";
import {
  Education,
  EducationEvent,
  EducationProvider,
  EnrichedCandidates,
  SearchedEducation
} from "../model/searched-education.model";
import {map} from "rxjs/operators";

@Injectable({
  providedIn: 'root'
})
export class FetchEducationService {

  constructor(private httpClient: HttpClient) {
  }

  private backendUrl = environment.settings.educationApi;

  public isLoading = false;

  public fetchEducation(educationId: string): Observable<SearchedEducation> {
  this.isLoading = true;
    return this.httpClient
      .get(this.backendUrl + '/v1/educations/' + educationId)
      .pipe(map(data => this.handleSearchedEducationResponse(data)));
  }

  public handleSearchedEducationResponse(fetchedEducationInput: any): SearchedEducation {
    const educationOut = new SearchedEducation();
    if (fetchedEducationInput) {
      educationOut.id = fetchedEducationInput['id'];

      let education = new Education();
      if (fetchedEducationInput['education']) {
        const educationIn = fetchedEducationInput['education'];
        if (educationIn['title']) {
          for (let educationTitle of educationIn['title']) {
            if (educationTitle['lang'] == 'swe') {
              education.title = educationTitle['content'];
            }
          }
        }

        education.code = educationIn['code'];

        if (educationIn['credits']) {
          const credits = educationIn['credits'];
          let hp = ''
          if (credits['system'] && credits['system']['code']) {
            hp = credits['system']['code'];
          }
          const credit = credits['credits'];
          education.credits = credit + " " + hp;
        }

        if (educationIn['configuration']) {
          education.type = educationIn['configuration']['code'];
        }
        if (educationIn['form']) {
          education.form = educationIn['form']['code'];
        }
        if (educationIn['description']) {
          for (let educationDescription of educationIn['description']) {
            if (educationDescription['lang'] == 'swe') {
              education.description = educationDescription['content'];
            }
          }
        }
        if (educationIn['eligibility'] && educationIn['eligibility']['eligibilityDescription']) {
          for (let eligibilityDescription of educationIn['eligibility']['eligibilityDescription']) {
            if (eligibilityDescription['lang'] == 'swe') {
              education.eligibility = eligibilityDescription['content'];
            }
          }
        }
        if (educationIn['educationLevel']) {
          education.level = educationIn['educationLevel']['code'];
        }

        if (educationIn['recommendedPriorKnowledge']) {
          for (let recommendedPriorKnowledge of educationIn['recommendedPriorKnowledge']) {
            if (recommendedPriorKnowledge['lang'] == 'swe') {
              education.recommendedPriorKnowledge = recommendedPriorKnowledge['content'];
            }
          }
        }
        education.resultIsDegree = educationIn['resultIsDegree'];

        educationOut.education = education;

      }

      if (fetchedEducationInput['events']) {
        for (let eventIn of fetchedEducationInput['events']) {

          let event = new EducationEvent();
          if (eventIn['execution']) {
            const executionIn = eventIn['execution'];
            event.executionStart = executionIn['start'];
            event.executionEnd = executionIn['end'];
          }

          event.itdistance = eventIn['itdistance'];

          if (eventIn['urls']) {
            for (let url of eventIn['urls']) {
              if (url['lang'] == 'swe') {
                event.url = url['content'];
              }
            }
          }

          event.cancelled = eventIn['cancelled'];
          event.languageOfInstruction = eventIn['languageOfInstruction'];

          if (eventIn['application'] && eventIn['application']['last']) {
            event.lastApplicationDate = eventIn['application']['last'];
          }

          if (eventIn['locations']) {
            for (let location of eventIn['locations']) {
              if (location['country'] == 'SE') {
                event.municipalityCode = location['municipalityCode']
                event.regionCode = location['regionCode']
              } else {
                console.warn('Swedish location not found')
              }
            }
          }

          if (eventIn['applicationDetails'] && eventIn['applicationDetails']['onlyAsPartOfProgram']) {
            event.onlyAsPartOfProgram = eventIn['applicationDetails']['onlyAsPartOfProgram'];
          }
          event.paceOfStudyPercentage = eventIn['paceOfStudyPercentage'];
          event.provider = eventIn['provider'];
          if (eventIn['timeOfStudy'] && eventIn['timeOfStudy']['code']) {
            event.timeOfStudy = eventIn['timeOfStudy']['code'];
          }
          if (eventIn['tuitionFee'] && eventIn['tuitionFee']['total']) {
            event.tuitionFee = eventIn['tuitionFee']['total'];
          }
          educationOut.events.push(event)
        }

      }

      if (fetchedEducationInput['education_providers']) {
        for (let providerIn of fetchedEducationInput['education_providers']) {
          let educationProvider = new EducationProvider();
          if (providerIn['name']) {
            for (let name of providerIn['name']) {
              if (name['lang'] == 'swe') {
                educationProvider.name = name['content'];
              }
            }
          }
          if (providerIn['urls']) {
            for (let url of providerIn['urls']) {
              if (url['lang'] == 'swe') {
                educationProvider.url = url['content'];
              }
            }
          }
          educationOut.providers.push(educationProvider);
        }
      }

      if (fetchedEducationInput['text_enrichments_results'] && fetchedEducationInput['text_enrichments_results']['enriched_candidates']) {
        const enrichedCandidatesIn = fetchedEducationInput['text_enrichments_results']['enriched_candidates'];

        const enrichedCompetencies = enrichedCandidatesIn['competencies'];
        const enrichedOccupations = enrichedCandidatesIn['occupations'];
        const enrichedCandidatesOut = new EnrichedCandidates();
        enrichedCandidatesOut.competencies = enrichedCompetencies;
        enrichedCandidatesOut.occupations = enrichedOccupations;
        educationOut.enrichedCandidates = enrichedCandidatesOut
      }
    }
    this.isLoading = false;
    return educationOut;
  }
}


