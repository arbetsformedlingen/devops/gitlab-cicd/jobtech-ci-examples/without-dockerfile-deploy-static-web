import {Injectable} from '@angular/core';
import {HttpClient, HttpErrorResponse} from "@angular/common/http";
import {environment} from "../../environments/environment";
import {MatchEducationsByJobtitleQuery} from "../model/search-queries.model";
import {
  EducationMatchResultLight, EducationMatchResultMetadata, JobtitlesAutocompleteItem,
  MatchedEducations, TermBoost
} from "../model/matched-educations-by-occupation.model";
import {Observable, of, throwError} from "rxjs";
import {catchError, map} from "rxjs/operators";

@Injectable({
  providedIn: 'root'
})
export class MatchEducationsByOccupationService {


  constructor(private httpClient: HttpClient) {
  }

  private backendUrl = environment.settings.educationApi;

  public isLoadingFirstPageResult: Boolean = false;
  public isLoadingPageResult: Boolean = false;

  private _nrDocsToFetch = 10;
  private _offsetToFetch = 0;

  public get nrDocsToFetch(): number {
    return this._nrDocsToFetch
  }

  private _matchEducationsQuery: MatchEducationsByJobtitleQuery = new MatchEducationsByJobtitleQuery();

  public isBackendError: Boolean = false;

  private _educationsByJobtitleResult: MatchedEducations = new MatchedEducations();

  public get totalHits(): number {
    if (this._educationsByJobtitleResult && this._educationsByJobtitleResult.totalHits) {
      return this._educationsByJobtitleResult.totalHits;
    } else {
      return 0;
    }
  }

  public get metadata(): EducationMatchResultMetadata | undefined {
    if (this._educationsByJobtitleResult && this._educationsByJobtitleResult.metadata) {
      return this._educationsByJobtitleResult.metadata;
    } else {
      return undefined;
    }
  }

  public get mappedOccupationLabel(): string {
    if (this._educationsByJobtitleResult && this._educationsByJobtitleResult.mappedOccupationLabel) {
      return this._educationsByJobtitleResult.mappedOccupationLabel;
    } else {
      return '';
    }
  }

  public get isFirstPage(): boolean {
    return this.page === 1;
  }

  public get page(): number {
    if (this._educationsByJobtitleResult && this._educationsByJobtitleResult.page) {
      return this._educationsByJobtitleResult.page
    } else {
      return 1
    }
  }

  public get totalPages(): number {
    if (this._educationsByJobtitleResult && this._educationsByJobtitleResult.totalPages) {
      return this._educationsByJobtitleResult.totalPages
    } else {
      return 1
    }
  }

  public get isLastPage(): boolean {
    const lastPage = this.page === this.totalPages;
    console.debug('isLastPage: ' + lastPage)
    return lastPage;
  }


  public get educationsBySearchResult(): Observable<MatchedEducations> {
    return of(this._educationsByJobtitleResult);
  }

  private setDefaultValuesForNewSearch(appendResult: boolean) {
    if (!appendResult) {
      this._offsetToFetch = 0;
      this._matchEducationsQuery.offset = 0;
      this.isLoadingFirstPageResult = true;
    }
    this.isLoadingPageResult = true;
    this.isBackendError = false;
  }

  public matchEducationsByJobtitle(matchEducationsQuery: MatchEducationsByJobtitleQuery, appendResult: boolean): void {
    this._matchEducationsQuery = matchEducationsQuery;
    this.setDefaultValuesForNewSearch(appendResult)
    if (matchEducationsQuery) {
      const url = `${this.backendUrl}/v1/educations/match-by-jobtitle`;

      this.httpClient.post(url, matchEducationsQuery)
        .pipe(catchError(this.handleError)).subscribe(data => {
        this._educationsByJobtitleResult = this.handleSearchedEducationsResponse(data, appendResult);
        this.isLoadingFirstPageResult = false;
        this.isLoadingPageResult = false;
      });
    }
  }


  public fetchAutocompleteJobtitles(word: string): Observable<JobtitlesAutocompleteItem[]> {
    //http://localhost:5000/jobtitles/autocomplete?word=tes&limit=10
    const autocompleteLimit = 10;
    const url = `${this.backendUrl}/v1/jobtitles/autocomplete?word=${word}&limit=${autocompleteLimit}`;
    return this.httpClient.get<JobtitlesAutocompleteItem[]>(url)
      .pipe(catchError(this.handleError)).pipe(map(data => {
        JobtitlesAutocompleteItem
        let dataIn: any = data
        const autocompleteItems = new Array<JobtitlesAutocompleteItem>();
        for (let item of dataIn) {
          let retItem = new JobtitlesAutocompleteItem();
          retItem.jobtitle = item['jobtitle'];
          retItem.occupation_id = item['occupation_id'];
          retItem.occupation_label = item['occupation_label'];

          autocompleteItems.push(retItem)
        }
        // console.log('this._jobtitlesAutocompleteResult:' + JSON.stringify(autocompleteItems)
        return autocompleteItems;

      }))
  }

  public fetchMoreSearchEducations(): void {
    this._offsetToFetch = this._offsetToFetch + this._nrDocsToFetch;
    console.log('fetchMoreSearchEducations: from ' + this._offsetToFetch);
    this._matchEducationsQuery.offset = this._offsetToFetch;
    this.matchEducationsByJobtitle(this._matchEducationsQuery, true);
  }

  handleError = (error: HttpErrorResponse) => {
    if (error.error instanceof ErrorEvent) {
      // A client-side or network error occurred. Handle it accordingly.
      console.error('An error occurred:', error.error.message);
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong.
      console.error(`Backend returned code ${error.status}, body was: ${error.error}`);
    }
    // this.isBackendError = true;
    // Return an observable with a user-facing error message.
    return throwError(
      'Something bad happened; please try again later.');
  }

  public handleSearchedEducationsResponse(educationsIn: any, appendResult: boolean): MatchedEducations {
    const educationsOut = new MatchedEducations();
    educationsOut.totalHits = 0;
    educationsOut.pageSize = 0;
    educationsOut.page = 1;
    educationsOut.totalPages = 1;
    educationsOut.mappedOccupationLabel = '';

    educationsOut.searchQuery = this._matchEducationsQuery;

    if (educationsIn) {
      let totalHits = 0
      if (educationsIn['hits_total']) {
        totalHits = educationsIn['hits_total']
      }
      educationsOut.totalHits = totalHits;

      if (educationsIn['mapped_occupation_for_match'] && educationsIn['mapped_occupation_for_match']['occupation_label']) {
        educationsOut.mappedOccupationLabel = educationsIn['mapped_occupation_for_match']['occupation_label'];
      }

      console.log('educationsOut.totalHits:' + educationsOut.totalHits);

      if (educationsIn.metadata) {
        console.log('educationsIn.metadata in match result.');
        educationsOut.metadata = educationsIn.metadata;
        if (educationsOut.metadata) {
          //Populate complex attribute types
          educationsOut.metadata.competencies_term_boost = this.populateTermBoosts(educationsIn.metadata.competencies_term_boost);
          educationsOut.metadata.occupations_term_boost = this.populateTermBoosts(educationsIn.metadata.occupations_term_boost);
        }
      }

      if (educationsIn.hits) {
        educationsOut.pageSize = educationsIn.hits.length
        let offset = 1;
        if (this._matchEducationsQuery.offset) {
          offset = this._matchEducationsQuery.offset
        }
        let limit = 1;
        if (this._matchEducationsQuery.limit) {
          limit = this._matchEducationsQuery.limit
        }
        console.log('offset: ' + offset);
        console.log('limit: ' + limit);
        educationsOut.page = Math.ceil(offset / limit)
        educationsOut.totalPages = Math.ceil(totalHits / limit)
        console.log('educationsOut.page:' + educationsOut.page);
        console.log('educationsOut.totalPages:' + educationsOut.totalPages);
      }
      const educations = new Array<EducationMatchResultLight>();
      if (educationsIn && educationsIn['hits']) {
        for (let hit of educationsIn['hits']) {
          // const educationIn = hit['education'];
          const education = new EducationMatchResultLight();
          if (hit['education_form']) {
            education.form = hit['education_form'];
          }
          if (hit['education_type']) {
            education.type = hit['education_type'];
          }
          if (hit['code']) {
            education.code = hit['code'];
          }

          if (hit['education_title']) {
            education.title = hit['education_title'];
          }
          education.id = hit['id'];
          if (hit['education_description']) {
            education.description = hit['education_description'];
          }
          if (hit['providerSummary']) {
            const providerSummaryIn = hit['providerSummary'];
            if (providerSummaryIn['providers']) {
              for (let provider of providerSummaryIn['providers']) {
                education.providers.push(provider);
              }
            }
          }
          if (hit['eventSummary']) {
            const eventSummaryIn = hit['eventSummary'];
            if (eventSummaryIn['municipalityCode']) {
              for (let municipalityCode of eventSummaryIn['municipalityCode']) {
                education.municipalityCodes.push(municipalityCode);
              }
            }
            if (eventSummaryIn['languageOfInstruction']) {
              for (let languageOfInstruction of eventSummaryIn['languageOfInstruction']) {
                education.languagesOfInstruction.push(languageOfInstruction);
              }
            }
            if (eventSummaryIn['onlyAsPartOfProgram']) {
              for (let onlyAsPartOfProgram of eventSummaryIn['onlyAsPartOfProgram']) {
                education.onlyAsPartOfPrograms.push(onlyAsPartOfProgram);
              }
            }
            if (eventSummaryIn['paceOfStudyPercentage']) {
              for (let paceOfStudyPercentage of eventSummaryIn['paceOfStudyPercentage']) {
                education.paceOfStudyPercentages.push(paceOfStudyPercentage);
              }
            }
            if (eventSummaryIn['timeOfStudy']) {
              for (let timeOfStudy of eventSummaryIn['timeOfStudy']) {
                education.timeOfStudies.push(timeOfStudy);
              }
            }
            if (eventSummaryIn['tuitionFee']) {
              for (let tuitionFee of eventSummaryIn['tuitionFee']) {
                education.tuitionFees.push(tuitionFee);
              }
            }
            if (eventSummaryIn['distance']) {
              education.distance = eventSummaryIn['distance'];
            }
          }
          educations.push(education);
        }
      }

      if (appendResult && this._educationsByJobtitleResult && this._educationsByJobtitleResult.educations) {
        const educationsMerged = this._educationsByJobtitleResult.educations.concat(educations)
        educationsOut.educations = educationsMerged;
      } else {
        educationsOut.educations = educations;
      }

    }
    return educationsOut;
  }

  private populateTermBoosts(dictionaryIn: any) {
    let competencies_term_boosts: TermBoost[] = new Array<TermBoost>()


    for (let termBoostKey in dictionaryIn) {
      let termBoostValue = dictionaryIn[termBoostKey];
      let termBoostObj = new TermBoost();
      termBoostObj.term = termBoostKey;
      termBoostObj.boost = termBoostValue;
      competencies_term_boosts.push(termBoostObj);
    }
    return competencies_term_boosts;
  }
}
