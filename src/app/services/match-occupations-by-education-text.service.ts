import {Injectable} from "@angular/core";
import {environment} from "../../environments/environment";
import {HttpClient, HttpErrorResponse} from "@angular/common/http";
import {Observable, of, throwError} from "rxjs";
import {
  IdentifiedKeywordsForInput,
  MatchedOccupations,
  OccupationGroup,
  RelatedOccupation, RelatedOccupationMetadata
} from "../model/matched-occupations.model";
import {catchError} from "rxjs/operators";
import {MatchOccupationsByEducationTextQuery} from "../model/search-queries.model";

@Injectable({
  providedIn: 'root'
})
export class MatchOccupationsByEducationTextService {
  private backendUrl = environment.settings.educationApi;

  public isLoadingPageResult: Boolean = false;
  public isSearchInitiated: Boolean = false;

  constructor(private httpClient: HttpClient) {
  }

  private _matchedOccupationsResult: MatchedOccupations = new MatchedOccupations();

  public get matchedOccupationsResult(): Observable<MatchedOccupations> {
    return of(this._matchedOccupationsResult);
  }

  public getMatchedOccupations(matchOccupationsByEducationTextQuery: MatchOccupationsByEducationTextQuery): void {
    this.isLoadingPageResult = true;
    this.isSearchInitiated = true;
    const url = `${this.backendUrl}/v1/occupations/match-by-text`;
    this.httpClient
      .post<MatchOccupationsByEducationTextQuery>(url, matchOccupationsByEducationTextQuery)
      .pipe(catchError(this.handleError)).subscribe(data => {
        this._matchedOccupationsResult = this.handleMatchedOccupationsResponse(data);
        this.isLoadingPageResult = false;
      }
    );
  }

  handleError = (error: HttpErrorResponse) => {
    if (error.error instanceof ErrorEvent) {
      // A client-side or network error occurred. Handle it accordingly.
      console.error('An error occurred:', error.error.message);
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong.
      console.error(`Backend returned code ${error.status}, body was: ${error.error}`);
    }
    // this.isBackendError = true;
    // Return an observable with a user-facing error message.
    return throwError(
      'Something bad happened; please try again later.');
  }

  public handleMatchedOccupationsResponse(occuationsIn: any): MatchedOccupations {
    const occuationsOut = new MatchedOccupations()
    occuationsOut.hitsTotal = occuationsIn['hits_total']
    occuationsOut.relatedoccupations = occuationsIn['hits_returned']
    occuationsOut.hitsReturned = occuationsIn['hits_returned']

    const occuationsInList = occuationsIn['related_occupations']
    const occuationsOutList: RelatedOccupation[] = [];

    for (let occuationIn of occuationsInList) {
      const occupation = new RelatedOccupation();
      const occupationGroupIn = occuationIn['occupation_group']
      const occupationGroupOut = new OccupationGroup()
      occupationGroupOut.occupationGroupLabel = occupationGroupIn['occupation_group_label']
      occupationGroupOut.conceptTaxonomyId = occupationGroupIn['concept_taxonomy_id']
      occupationGroupOut.ssyk = occupationGroupIn['ssyk']
      occupation.occupationGroup = occupationGroupOut
      occupation.occupationLabel = occuationIn['occupation_label']
      occupation.id = occuationsIn['id']
      occupation.conceptTaxonomyId = occuationIn['concept_taxonomy_id']
      occupation.legacyAmsTaxonomyId = occuationIn['legacy_ams_taxonomy_id']

      if (occuationIn['metadata']) {
        const occupationMetadataIn = occuationIn['metadata']
        const occupationMetadata = new RelatedOccupationMetadata();
        occupationMetadata.match_score = occupationMetadataIn['match_score']
        occupationMetadata.enriched_ads_count = occupationMetadataIn['enriched_ads_count']
        occupationMetadata.enriched_ads_percent_of_total = Math.round((occupationMetadataIn['enriched_ads_percent_of_total'] + Number.EPSILON) * 1000) / 1000

        occupation.metadata = occupationMetadata
      }

      occuationsOutList.push(occupation);
    }
    occuationsOut.relatedoccupations = occuationsOutList

    if (occuationsIn['identified_keywords_for_input']) {
      let identifiedKeywords = new IdentifiedKeywordsForInput();
      const identifiedKeywordsIn = occuationsIn['identified_keywords_for_input'];

      if (identifiedKeywordsIn['competencies']) {
        for (let competence of identifiedKeywordsIn['competencies']) {
          identifiedKeywords.competencies.push(competence);
        }
      }
      if (identifiedKeywordsIn['occupations']) {
        for (let occupation of identifiedKeywordsIn['occupations']) {
          identifiedKeywords.occupations.push(occupation);
        }
      }
      occuationsOut.identifiedKeywordsForInput = identifiedKeywords;
    }

    return occuationsOut
  }
}
