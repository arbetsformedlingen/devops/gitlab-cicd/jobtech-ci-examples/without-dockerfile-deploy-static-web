import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class NavbarService {
  get displayMatchedOccupations(): boolean {
    return this._displayMatchedOccupations;
  }

  set displayMatchedOccupations(value: boolean) {
    this._displayMatchedOccupations = value;
  }
  get displayEducationDetails(): boolean {
    return this._displayEducationDetails;
  }

  set displayEducationDetails(value: boolean) {
    this._displayEducationDetails = value;
  }
  private searchEnabled = false;
  private _displayMatchedOccupations = false;
  private _displayEducationDetails = false;

  constructor() {
  }

  isSearchEnabled(): boolean {
    return this.searchEnabled;
  }

  enableSearch(): void {
    this.searchEnabled = true;
  }

  disableSearch() {
    this.searchEnabled = false;
  }



}
