import {Observable, of, throwError} from 'rxjs';
import {map} from 'rxjs/operators';
import {Injectable} from '@angular/core';
import {HttpClient, HttpErrorResponse, HttpHeaders} from '@angular/common/http';
import {catchError, retry} from 'rxjs/operators';
import {environment} from "../../environments/environment";
import {MatchedOccupations, OccupationGroup, RelatedOccupation} from "../model/matched-occupations.model";
import {EducationLight, SearchedEducations} from "../model/searched-educations.model";
import {SearchEducationsQuery} from "../model/search-queries.model";
import {SearchedEducation} from "../model/searched-education.model";

@Injectable({
  providedIn: 'root'
})
export class SearchEducationsService {

  constructor(private httpClient: HttpClient) {
  }

  private backendUrl = environment.settings.educationApi;

  public isLoadingFirstPageResult: Boolean = false;
  public isLoadingPageResult: Boolean = false;

  private _nrDocsToFetch = 10;
  private _pageToFetch = 0;

  public get nrDocsToFetch(): number {
    return this._nrDocsToFetch
  }

  private _searchEducationsQuery: SearchEducationsQuery = new SearchEducationsQuery();

  public isBackendError: Boolean = false;

  public get isFirstPage(): boolean {
    return this.page === 1;
  }

  public get page(): number {
    if (this._educationsBySearchResult && this._educationsBySearchResult.page) {
      return this._educationsBySearchResult.page
    } else {
      return 1
    }
  }

  public get totalPages(): number {
    if (this._educationsBySearchResult && this._educationsBySearchResult.totalPages) {
      return this._educationsBySearchResult.totalPages
    } else {
      return 1
    }
  }

  public get isLastPage(): boolean {
    const lastPage = this.page === this.totalPages;
    console.debug('isLastPage: ' + lastPage)
    return lastPage;
  }

  public get totalHits():number {
    if (this._educationsBySearchResult && this._educationsBySearchResult.totalHits) {
      return this._educationsBySearchResult.totalHits;
    } else {
      return 0;
    }
  }

  private _educationsBySearchResult: SearchedEducations = new SearchedEducations();

  public get educationsBySearchResult(): Observable<SearchedEducations> {
    return of(this._educationsBySearchResult);
  }

  private setDefaultValuesForNewSearch(appendResult: boolean) {
    if (!appendResult) {
      this._pageToFetch = 0;
      this.isLoadingFirstPageResult = true;
    }
    this.isLoadingPageResult = true;
    this.isBackendError = false;
  }

  // 'https://education-api-ed-api-develop.test.services.jtech.se/educations?query=system&education_type=program&education_form=h%C3%B6gskoleutbildning
  public searchEducations(searchEducationsQuery: SearchEducationsQuery, appendResult: boolean): void {
    this._searchEducationsQuery = searchEducationsQuery;
    this.setDefaultValuesForNewSearch(appendResult)
    if (searchEducationsQuery) {
      let url = `${this.backendUrl}/v1/educations?query=${searchEducationsQuery.freeText}`
      url += `${SearchEducationsService.createListInputParameter('education_type', searchEducationsQuery.educationType)}`
      url += `${SearchEducationsService.createListInputParameter('education_form', searchEducationsQuery.educationForm)}`
      url += `&municipality_code=${searchEducationsQuery.municipalityCode}`
      url += `&region_code=${searchEducationsQuery.regionCode}`
      url += `&pace_of_study_percentage=${searchEducationsQuery.paceOfStudyPercentage}`
      url += `&education_code=${searchEducationsQuery.educationCode}`
      url += `&distance=${searchEducationsQuery.distance}`
      url += `&filter_education_plan_exists=${searchEducationsQuery.educationPlanExists}`
      url += `&limit=${this._nrDocsToFetch}&offset=${this._pageToFetch}`
      this.httpClient.get(url)
        .pipe(catchError(this.handleError)).subscribe(data => {
        this._educationsBySearchResult = this.handleSearchedEducationsResponse(data, appendResult);
        this.isLoadingFirstPageResult = false;
        this.isLoadingPageResult = false;
      });
    }
  }

  private static createListInputParameter(name: string, values: any): string {
    let inputParameters = [];
    for (let val of values) {
      inputParameters.push(`&${name}=${val}`)
    }
    return inputParameters.join('')
  }

  public fetchMoreSearchEducations(): void {
    this._pageToFetch = this._pageToFetch + this._nrDocsToFetch;
    console.log('fetchMoreSearchEducations: from ' + this._pageToFetch);
    this.searchEducations(this._searchEducationsQuery, true);
  }

  handleError = (error: HttpErrorResponse) => {
    if (error.error instanceof ErrorEvent) {
      // A client-side or network error occurred. Handle it accordingly.
      console.error('An error occurred:', error.error.message);
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong.
      console.error(`Backend returned code ${error.status}, body was: ${error.error}`);
    }
    // this.isBackendError = true;
    // Return an observable with a user-facing error message.
    return throwError(
      'Something bad happened; please try again later.');
  }

  public handleSearchedEducationsResponse(educationsIn: any, appendResult: boolean): SearchedEducations {
    const educationsOut = new SearchedEducations();
    educationsOut.totalHits = 0
    educationsOut.pageSize = 0
    educationsOut.page = 1
    educationsOut.totalPages = 1

    educationsOut.searchQuery = this._searchEducationsQuery

    if (educationsIn) {
      let totalHits = 0
      if (educationsIn['hits']) {
        totalHits = educationsIn['hits']
      }
      educationsOut.totalHits = totalHits;
      if (educationsIn.result) {
        educationsOut.pageSize = educationsIn.result.length
        let offset = 0;
        if (this._searchEducationsQuery.offset) {
          offset = this._searchEducationsQuery.offset
        }
        let limit = 1;
        if (this._searchEducationsQuery.limit) {
          limit = this._searchEducationsQuery.limit
        }
        educationsOut.page = Math.ceil(offset / limit)
        educationsOut.totalPages = Math.ceil(totalHits / limit)
      }
      const educations = new Array<EducationLight>();
      if (educationsIn && educationsIn['result']) {
        for (let result of educationsIn['result']) {
          const educationIn = result['education'];
          const education = new EducationLight();
          if (educationIn['form']) {
            education.form = educationIn['form']['code'];
          }
          if (educationIn['configuration']) {
            education.type = educationIn['configuration']['code'];
          }
          education.code = educationIn['code'];
          if (educationIn['title']) {
            for (let educationTitle of educationIn['title']) {
              if (educationTitle['lang'] == 'swe') {
                education.title = educationTitle['content'];
              }
            }
          }
          education.identifier = educationIn['identifier'];
          if (educationIn['description']) {
            for (let educationDescription of educationIn['description']) {
              if (educationDescription['lang'] == 'swe') {
                education.description = educationDescription['content'];
              }
            }
          }
          if (result['providerSummary']) {
            const providerSummaryIn = result['providerSummary'];
            if (providerSummaryIn['providers']) {
              for (let provider of providerSummaryIn['providers']) {
                education.providers.push(provider);
              }
            }
          }
          if (result['eventSummary']) {
            const eventSummaryIn = result['eventSummary'];
            if (eventSummaryIn['municipalityCode']) {
              for (let municipalityCode of eventSummaryIn['municipalityCode']) {
                education.municipalityCodes.push(municipalityCode);
              }
            }
            // Prepare for fetch regions, even if not implemented in view...
            if (eventSummaryIn['regionCode']) {
              for (let regionCode of eventSummaryIn['regionCode']) {
                education.regionCodes.push(regionCode);
              }
            }
            if (eventSummaryIn['languageOfInstruction']) {
              for (let languageOfInstruction of eventSummaryIn['languageOfInstruction']) {
                education.languagesOfInstruction.push(languageOfInstruction);
              }
            }
            if (eventSummaryIn['onlyAsPartOfProgram']) {
              for (let onlyAsPartOfProgram of eventSummaryIn['onlyAsPartOfProgram']) {
                education.onlyAsPartOfPrograms.push(onlyAsPartOfProgram);
              }
            }
            if (eventSummaryIn['paceOfStudyPercentage']) {
              for (let paceOfStudyPercentage of eventSummaryIn['paceOfStudyPercentage']) {
                education.paceOfStudyPercentages.push(paceOfStudyPercentage);
              }
            }
            if (eventSummaryIn['timeOfStudy']) {
              for (let timeOfStudy of eventSummaryIn['timeOfStudy']) {
                education.timeOfStudies.push(timeOfStudy);
              }
            }
            if (eventSummaryIn['tuitionFee']) {
              for (let tuitionFee of eventSummaryIn['tuitionFee']) {
                education.tuitionFees.push(tuitionFee);
              }
            }
            if (eventSummaryIn['distance']) {
              education.distance = eventSummaryIn['distance'];
            }
          }
          educations.push(education);
        }
      }

      if (appendResult && this._educationsBySearchResult && this._educationsBySearchResult.educations) {
        const educationsMerged = this._educationsBySearchResult.educations.concat(educations)
        educationsOut.educations = educationsMerged;
      } else {
        educationsOut.educations = educations;
      }

    }
    return educationsOut;
  }

}
